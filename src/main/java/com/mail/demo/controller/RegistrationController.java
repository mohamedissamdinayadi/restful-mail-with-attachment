package com.mail.demo.controller;

import com.mail.demo.model.User;
import com.mail.demo.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.logging.Logger;

/**
 * This class contains a Mail API developed using Spring Boot
 *
 * @author MukulJaiswal
 */
@RestController
public class RegistrationController {

    @Autowired
    private MailService notificationService;

    @Autowired
    private User user;


    @RequestMapping("send-mail")
    public String send() {

        /*
         * Creating a User with the help of User class that we have declared. Setting
         * the First,Last and Email address of the sender.
         */
        user.setFirstName("med");
        user.setLastName("issam");
        user.setEmailAddress("mohamedissamdinayadi@gmail.com"); //Receiver's email address

        /*
         * Here we will call sendEmail() for Sending mail to the sender.
         */
        try {
            notificationService.sendEmail(user);
        } catch (MailException mailException) {
            System.out.println(mailException);
        }
        return "Congratulations! Your mail has been send to the user.";
    }


    @RequestMapping("send-mail-attachment")
    public String sendWithAttachment() throws MessagingException {

        /*
         * Creating a User with the help of User class that we have declared. Setting
         * the First,Last and Email address of the sender.
         */
        user.setFirstName("issam");
        user.setLastName("med");
        user.setEmailAddress("mohamedissamdinayadi@gmail.com"); //Receiver's email address

        /*
         * Here we will call sendEmailWithAttachment() for Sending mail to the sender
         * that contains a attachment.
         */
        try {
//            notificationService.sendEmailWithAttachment(user);
        } catch (MailException mailException) {
            System.out.println(mailException);
        }
        return "Congratulations! Your mail has been send to the user.";
    }





    	private static final Logger logger = Logger.getLogger(RegistrationController.class.getName());
    @PostMapping("/upload")
    public ResponseEntity<String> uploadData(@RequestParam("file") MultipartFile file) throws Exception {
        if (file == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }
        InputStream inputStream = file.getInputStream();
        String originalName = file.getOriginalFilename();
        String name = file.getName();
        String contentType = file.getContentType();
        long size = file.getSize();
		logger.info("inputStream: " + inputStream);
		logger.info("originalName: " + originalName);
		logger.info("name: " + name);
		logger.info("contentType: " + contentType);
		logger.info("size: " + size);
//        // Do processing with uploaded file data in Service layer
        user.setFirstName("issam");
        user.setLastName("med");
        user.setEmailAddress("mohamedissamdinayadi@gmail.com"); //Receiver's email address

        notificationService.sendEmailWithAttachment(user , file , originalName);

        return new ResponseEntity<String>(originalName, HttpStatus.OK);

    }


}
